package API.Entities.ConfigEntity;

public class Autocomplete {
    private String baseurl;

    private String apikeyname;

    private String apikeyvalue;

    public void setBaseurl(String baseurl){
        this.baseurl = baseurl;
    }
    public String getBaseurl(){
        return this.baseurl;
    }
    public void setApikeyname(String apikeyname){
        this.apikeyname = apikeyname;
    }
    public String getApikeyname(){
        return this.apikeyname;
    }
    public void setApikeyvalue(String apikeyvalue){
        this.apikeyvalue = apikeyvalue;
    }
    public String getApikeyvalue(){
        return this.apikeyvalue;
    }
}
