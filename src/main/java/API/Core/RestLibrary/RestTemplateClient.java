package API.Core.RestLibrary;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateClient implements IRestClient {

    private final RestTemplate template = new RestTemplate();

    /**
     * Executes GET verb over the URL
     * @param path : Path to get the query
     * @param classArgument class type: Example: String.class
     * @param <T> Type parameter
     * @return Object as T class
     */
    @Override
    public <T> T get(String path, MultiValueMap<String, String> headers, Class<T> classArgument) {
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<T> result = template.exchange(path, HttpMethod.GET, entity, classArgument);

        return result.getBody();
    }
}
