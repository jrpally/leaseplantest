package API.Core.RestLibrary;

import io.restassured.response.Response;
import org.springframework.util.MultiValueMap;

import java.net.URISyntaxException;
import java.util.HashMap;

public interface IRestClient {
    /**
     * Rest Client Generic / Rest Client Abstraction independent of Technology
     * @param path Base URl
     * @param classArgument Type to Get
     * @param <T> Type Argument
     * @return Return Value
     */
    <T> T get(String path, MultiValueMap<String, String> headers, Class<T> classArgument);
}
