package API.Core.ApiFacade;

import API.Core.Configuration.IApiConfiguration;
import API.Core.RestLibrary.IRestClient;
import API.Entities.ImdbEntities.MatchingMovies;
import org.apache.http.client.utils.URIBuilder;

public class AutoCompleteAPI {

    private final IApiConfiguration configuration;
    private final IRestClient restClient;

    public AutoCompleteAPI(IRestClient restClient,
                           IApiConfiguration configuration) {
        if (restClient == null) {
            throw new IllegalArgumentException("Rest Client is null");
        }
        if (configuration == null) {
            throw new IllegalArgumentException("Configuration is null");
        }
        this.restClient = restClient;
        this.configuration = configuration;
    }

    public MatchingMovies searchMovie(String value) {
        URIBuilder searchMovieUri = this.configuration.getBaseUri();
        searchMovieUri.addParameter("q", value);

        return restClient.get(searchMovieUri.toString(),
                this.configuration.getHeaders(),
                MatchingMovies.class);
    }
}
