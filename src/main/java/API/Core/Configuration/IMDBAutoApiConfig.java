package API.Core.Configuration;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.net.URISyntaxException;


public class IMDBAutoApiConfig implements IApiConfiguration {

    private final ImdbProperties imdbProperties;


    public IMDBAutoApiConfig(PropertyFile propertyFile) {
        if (propertyFile == null) {
            throw new IllegalArgumentException("Property file is not valid");
        }
        this.imdbProperties = propertyFile.loadConfiguration(ImdbProperties.class);
    }
    @Override
    public MultiValueMap<String, String> getHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        headers.add(imdbProperties.getApiservice().getImdb().getAutocomplete().getApikeyname(),
                imdbProperties.getApiservice().getImdb().getAutocomplete().getApikeyvalue());
        return headers;
    }

    @Override
    public URIBuilder getBaseUri() {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(imdbProperties.getApiservice().getImdb().getAutocomplete().getBaseurl());
        } catch (URISyntaxException e) {
            throw new RuntimeException("Unable to parse URL");
        }
        return uriBuilder;
    }
}
