package API.Core.Configuration;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.util.MultiValueMap;

public interface IApiConfiguration {
    MultiValueMap<String, String> getHeaders();

    URIBuilder getBaseUri();
}
