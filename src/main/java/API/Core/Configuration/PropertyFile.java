package API.Core.Configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

public class PropertyFile {
    private final String path;

    public PropertyFile(String path) {
        if (path == null) {
            throw new IllegalArgumentException("Unable to read path");
        }
        this.path = path;
    }

    public <T> T loadConfiguration(Class<T> classType) {
        String fileContent = getFileContent();
        JavaPropsMapper objectMapper = new JavaPropsMapper();
        try {
            return objectMapper.readValue(fileContent, classType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getFileContent() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.path)).getFile());

        if (!file.exists()) {
            throw new RuntimeException("File not found " + this.path);

        }
        //Read File Content
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
