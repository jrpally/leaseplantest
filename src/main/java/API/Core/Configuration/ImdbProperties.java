package API.Core.Configuration;

import API.Entities.ConfigEntity.Apiservice;

public class ImdbProperties {
    private Apiservice apiservice;

    public void setApiservice(Apiservice apiservice){
        this.apiservice = apiservice;
    }
    public Apiservice getApiservice(){
        return this.apiservice;
    }
}
