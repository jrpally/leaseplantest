package stepsDefinitions;

import API.Core.ApiFacade.AutoCompleteAPI;
import API.Core.Configuration.IApiConfiguration;
import API.Core.Configuration.IMDBAutoApiConfig;
import API.Core.Configuration.PropertyFile;
import API.Core.RestLibrary.IRestClient;
import API.Core.RestLibrary.RestTemplateClient;
import API.Entities.ImdbEntities.MatchingMovies;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static org.hamcrest.MatcherAssert.assertThat;

public class stepDefinitions {

    private MatchingMovies matchingMovies;
    private String stringValue;
    private final AutoCompleteAPI autoCompleteAPI;

    public stepDefinitions() {
        PropertyFile resourceFile = new PropertyFile("application.properties");
        IApiConfiguration autoCompleteConfig = new IMDBAutoApiConfig(resourceFile);
        IRestClient restClient = new RestTemplateClient();

        this.autoCompleteAPI
                = new AutoCompleteAPI(restClient, autoCompleteConfig);
    }

    @Given("^I search a movie partially with the value \"([^\"]*)\"$")
    public void iPerformGETOperationForWithTheValue(String value) {
        matchingMovies = this.autoCompleteAPI.searchMovie(value);
    }

    @Then("^Any movie should (be|not be) like \"([^\"]*)\"$")
    public void theResponseStatusCodeShouldBe(String isInserted, String movie) {
        boolean isParameterOnTable = isInserted.equals("be");
        if (isParameterOnTable) {
            assertThat("Verify movie", matchingMovies.d.get(0).l.contains(movie));
            return;
        }
        assertThat("Verify movie", !matchingMovies.d.get(0).l.contains(movie));

    }
}
