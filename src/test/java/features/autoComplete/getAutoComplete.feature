@smoke
Feature: GET

  Scenario: Verify that the movie exists
    Given I search a movie partially with the value "Iron Man"
    Then Any movie should be like "Iron Man"

  Scenario: Verify that the movie with an invalid name does not exist
    Given I search a movie partially with the value "4545"
    Then Any movie should not be like "Iron Man"
