package API;

import API.Core.ApiFacade.AutoCompleteAPI;
import API.Core.Configuration.IApiConfiguration;
import API.Core.Configuration.IMDBAutoApiConfig;
import API.Core.Configuration.PropertyFile;
import API.Core.RestLibrary.IRestClient;
import API.Core.RestLibrary.RestTemplateClient;
import API.Entities.ImdbEntities.MatchingMovies;
import junit.framework.TestCase;
import org.junit.Assert;

public class AutoCompleteAPITest extends TestCase {

    public void testSearchMovie() {
        PropertyFile resourceFile = new PropertyFile("application.properties");
        IApiConfiguration autoCompleteConfig = new IMDBAutoApiConfig(resourceFile);
        IRestClient restClient = new RestTemplateClient();

        AutoCompleteAPI autoCompleteAPI
                = new AutoCompleteAPI(restClient, autoCompleteConfig);

        MatchingMovies matchingMovies = autoCompleteAPI.searchMovie("Lord of the rings");

        Assert.assertTrue(matchingMovies.d.get(0).i.imageUrl.contains("http"));
    }
}