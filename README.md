# LeasePlan

API Automation Framwork built using RestTemplate, Java, Maven, serenity-cucumber.

- The public API used for this example is - https://rapidapi.com/apidojo/api/imdb8
- This framework is developed using RestTemplate library for Rest API testing
- Reporting is by serenity-cucumber

## Requirements

- Apache Maven 3.8.1
- spring-web 5.3.6
- serenity-cucumber 1.6.4

## Steps to run the test cases

- Install the mvn dependencies:
```sh
mvn clean install
```
- Run test cases to generate serenity-cucumber:
```sh
mvn clean verify
```

## Serenity report 

- Go to CI/CD Pipelines: https://gitlab.com/jrpally/leaseplantest/-/pipelines
- Download the serenity-cucumber folder:
![plot](./gitlab/pipelineDownload.jpg)
![plot](./gitlab/downloadArchive.jpg)
- In the downloaded Zip file, go to following folder:
```sh
/target/site/serenity/
```
- Open the index.html file:

![plot](./gitlab/serenityBDD.jpg)

